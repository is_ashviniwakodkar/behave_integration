Feature: Pagely site verification
 Scenario: Navigation to sign up page
  Given launch Pagely home page
  When user clicks on sign up button
  Then verify that navigates on sign up page
  And close browser

 Scenario: Invalid sign in verification
  Given launch Pagely home page
  When user navigates to sign in page and enter details
  Then verify incorrect credentials error message
  And close browser

 Scenario: Drop down selection
  Given launch Pagely home page
  When user clicks on Enterprises option under Hosting Solutions dropdown
  Then user navigates to Enterprises page
  And close browser

 Scenario: Captcha error message validation
  Given launch Pagely home page
  When user clicks on Contact option and select General sales
  Then user should enter details and click submit
  Then user should verify captach invalid error message
  And close browser

 Scenario: Link validation displayed or not
  Given launch Pagely home page 
  When user verify all Link
  Then user should verify link are correct
  And close browser
