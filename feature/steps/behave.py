import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from behave import given, when, then, step

driver = None
firstLink=None
secondLink=None
thirdLink=None
fourthLink=None
fifthLink=None
sixthLink=None

@given('launch Pagely home page')
def launchHomePage(context):
    context.driver=webdriver.Chrome(ChromeDriverManager().install())
    #driver = webdriver.Chrome(executable_path= r'C:\\Users\\ashvini.local\\Downloads\\chromedriver_win32\\chromedriver.exe')
    context.driver.get("https://pagely.com/")

@when('user clicks on sign up button')
def openHomePage(context):
    context.driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[2]/a").click()

@then('verify that navigates on sign up page')
def verifySignUpPage(context):
    elem = context.driver.find_element(By.XPATH, "//*[@id='signup_content']/div[2]/div[2]/div/div/div[1]/h1")
    assert elem.text =="CHOOSE A PLAN", "header is not correct"

@when('user navigates to sign in page and enter details')
def openSignInPage(context):
    context.driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[1]/a").click()
    elem = context.driver.find_element(By.XPATH, "//*[@id='app']/div/div/div[2]")
    assert elem.text=="WELCOME TO ATOMIC!", "Navigation failed to login page"
    username = context.driver.find_element(By.XPATH,"//*[@id='username']")
    username.send_keys("abcd")
    password = context.driver.find_element(By.XPATH,"//*[@id='password']")
    password.send_keys("abcd1")
    context.driver.find_element(By.XPATH, "//div/div[2]/button/span[1]").click()

@then('verify incorrect credentials error message')
def verifyInvalidCredentials(context):
    elem = context.driver.find_element(By.XPATH, "//div/div[2]/div[4]/div")
    assert elem.text, "incorrect credentials"

@when('user clicks on Enterprises option under Hosting Solutions dropdown')
def hostingDropdown(context):
    context.driver.find_element(By.XPATH, "//*[@id='menu-item-9576']/a").click()
    context.driver.find_element(By.XPATH, "//*[@id='menu-item-9576']/div/a[text()='Enterprise']").click()

@then('user navigates to Enterprises page')
def enterprisePage(context):
    elem = context.driver.find_element(By.XPATH,"//*[@id='pagely_hero']/div[7]/div/div/div/h1")
    assert elem.text == "ENTERPRISE WORDPRESS HOSTING", "navigation failed"

@when('user clicks on Contact option and select General sales')
def contactOption(context):
    context.driver.find_element(By.XPATH, "//li/a[text()='Contact']").click()
    context.driver.find_element(By.XPATH, "//*[@id='field_13_9']/div/div").click()
    context.driver.find_element(By.XPATH, "//li[2]/span[text()='General Sales']").click()

@then('user should enter details and click submit')
def enterDetails(context):
    name = context.driver.find_element(By.XPATH,"//*[@id='input_13_7']")
    name.send_keys("abcd")
    email = context.driver.find_element(By.XPATH,"//*[@id='input_13_3']")
    email.send_keys("abcd1@gmail.com")
    msg = context.driver.find_element(By.XPATH,"//*[@id='input_13_4']")
    msg.send_keys("first user")
    context.driver.find_element(By.XPATH, "//input[@id='gform_submit_button_13']").click()

@then('user should verify captach invalid error message')
def captachError(context):
    captcha = context.driver.find_element(By.XPATH,"//*[@id='validation_message_13_15']")
    assert captcha.text == "The reCAPTCHA was invalid. Go back and try it again.", "validation failed"

@when('user verify all Link')
def hostingLink(context):
    context.firstLink = context.driver.find_element(By.XPATH,"//a[text()='Hosting Solutions']")
   # assert firstLink.text == "HOSTING SOLUTIONS", "link is not correct"
    context.secondLink = context.driver.find_element(By.XPATH,"//*[@id='menu-item-20995']/a")
    #assert secondLink.text == "LEARN", "link is not correct"
    context.thirdLink = context.driver.find_element(By.XPATH,"//*[@id='menu-item-9577']/a")
   # assert thirdLink.text == "PRICING", "link is not correct"
    context.fourthLink = context.driver.find_element(By.XPATH,"//*[@id='menu-item-34']/a")
    #assert fourthLink.text == "SUPPORT", "link is not correct"
    context.fifthLink = context.driver.find_element(By.XPATH, "//li/a[text()='Contact']")
    #assert fifthLink.text == "CONTACT", "link is not correct"
    context.sixthLink = context.driver.find_element(By.XPATH, "//*[@id='navbarContent']/ul[2]/li[1]/a")
    #assert sixthLink.text == "SIGN IN", "link is not correct"

@then('user should verify link are correct')
def verifylinkVisible(context):
    assert context.firstLink.text == "HOSTING SOLUTIONS", "link is not correct"
    assert context.secondLink.text == "LEARN", "link is not correct"
    assert context.thirdLink.text == "PRICING", "link is not correct"
    assert context.fourthLink.text == "SUPPORT", "link is not correct"
    assert context.fifthLink.text == "CONTACT", "link is not correct"
    assert context.sixthLink.text == "SIGN IN", "link is not correct"

@then('close browser')
def closeBrowser(context):
    context.driver.close()